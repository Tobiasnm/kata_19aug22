import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Stream;

public class main {
    public static void main(String[] args) {
        LocalDate startDate = LocalDate.parse("2000-01-01");
        List<LocalDate> listOfDates = Stream.iterate(startDate, date -> date.plusDays(1))
                .filter(date -> date.getDayOfWeek() == DayOfWeek.FRIDAY)
                .filter(date -> date.getDayOfMonth() == 13)
                .limit(50).toList();
        for (LocalDate l: listOfDates) {
            System.out.println(l);
        }
    }
}